#include <iostream>
#include <string>

int main()
{
    std::string example = "Example string";
    std::cout << example << "\n";

    std::cout << example.length() << "\n"; // Output stings length

    char const& front = example.front();
    std::cout << front << '\n'; // Output first symbol of string - 'E'

    char const& back = example.back();
    std::cout << back << '\n'; // Output last symbol of string - 'g'


    return 0;
}